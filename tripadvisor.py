
import requests
from bs4 import BeautifulSoup

URL_PAGE_1 = "https://www.tripadvisor.com/Restaurants-g293984-Tel_Aviv_Tel_Aviv_District.html"
URL_PAGE_N = "https://www.tripadvisor.com/RestaurantSearch-g293984-oa|N|-Tel_Aviv_Tel_Aviv_District.html#EATERY_LIST_CONTENTS"

def get_url(n):
    if n == 1:
        return URL_PAGE_1
    else:
        url_split = URL_PAGE_N.split('|')
        url_split[1] = str((n-1)*30)
    return ''.join(url_split)


def get_features(url):
    """This function loop over all the information we need to create a database on Tripadvisor website"""
    source = requests.get(url)
    soup = BeautifulSoup(source.content, 'html.parser')

    final = []

    for res in range(2,31):
        try:
            listing = "listing rebrand listingIndex-2"
            listing = listing.replace(listing[-1], str(res))
            containers = soup.find('div', class_=str(listing))
            parent = containers.find('div', class_="ui_columns is-mobile")
            details = parent.find('div', class_="ui_column is-9 shortSellDetails")

            title = details.find('div', class_='title').get_text().strip()
            info1 = details.find('div', class_='rating rebrand')
            reviews = details.find('div', class_='rating rebrand').get_text().strip()
            rating = info1.find('span')['alt'].strip()

            index = details.find('div', class_='popIndexBlock').get_text()[:5].strip()

            info2 = details.find('div', class_='cuisines')
            price = info2.find('span', class_='item price').get_text().strip()
            category = info2.find('a', class_='item cuisine').get_text().strip()
            category2 = info2.find('span', class_='item cuisine').get_text().strip()
            cuisine = (category, category2)

            features = [title, reviews, rating, index, price, cuisine]

            final.append(features)
        except:
            print("")

    return final

print(get_url(17))


